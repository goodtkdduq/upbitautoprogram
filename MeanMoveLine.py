import pandas_datareader as pdr
from pandas import DataFrame
import datetime
import time
import math


class MeanMoveLine:
    moveLine = {}
    moveLineForVolume = {}

    # 종목 / 코스닥,코스피( KS or KQ ) / 몇년치 / 몇개월치 / 몇일치
    def __init__(self, jongmok=None, type=None, howManyYear=None, howManyMonth=None, howMany=None, data=None,
                 sqlData=True, reverse=True, useVolume=False):
        self.moveLine.clear()
        self.moveLineForVolume.clear()
        # if data != None:

        if sqlData == True:
            self.data = data
            self.data['5 이평선'] = None
            self.data['10 이평선'] = None
            self.data['20 이평선'] = None
            if reverse == True:
                self.data.sort_index(ascending=True, inplace=True)
            # 종가 데이터
            self.list = []
            real = self.getRealData()
            self.list.append([real, '^r--'])

            # 500초 평균선
            first = self.makeMeanData(5)
            self.list.append([first, 'g'])

            # 5000초 평균선
            sec = self.makeMeanData(10)
            self.list.append([sec, 'b'])

            # 20일 평균선

            th = self.makeMeanData(20)
            self.list.append([th, 'g'])

            if useVolume == True:
                self.volume = []
                self.data['5 거래량'] = None
                self.data['10 거래량'] = None
                self.data['20 거래량'] = None

                th = self.makeMeanVolume(5)
                self.volume.append([th, 'g'])

                th = self.makeMeanVolume(10)
                self.volume.append([th, 'b'])

                th = self.makeMeanVolume(20)
                self.volume.append([th, 'g'])

                self.makeMoveLineForVolume(self.volume)

            self.makeMoveLine(self.list)
            # self.getDeviation(self.list)
            # self.makeGrape(self.list)

        else:
            tm = datetime.datetime.now()
            startTm = str(tm.year - howManyYear) + "-" + str(tm.month - howManyMonth) + "-" + str(tm.day - howMany)
            endTm = str(tm.year) + "-" + str(tm.month) + "-" + str(tm.day)

            # startTm 부터 endTm까지의 주식 데이터 가져오기
            self.data = pdr.get_data_yahoo(jongmok + '.' + type, start=startTm, end=endTm)

            print(self.data)

            self.list = []

            # 종가 데이터
            real = self.getRealData()
            self.list.append([real, '^r--'])

            # 5일 평균선
            first = self.makeMeanData(5)
            self.list.append([first, 'g'])

            # 10일 평균선
            sec = self.makeMeanData(10)
            self.list.append([sec, 'b'])

            # 20일 평균선
            th = self.makeMeanData(20)
            self.list.append([th, 'y'])

            self.getDeviation(self.list)
            self.makeGrape(self.list)

    def makeMoveLine(self, list):

        for i in list:
            for j in i[0].keys():
                if self.moveLine.__contains__(j):
                    self.moveLine[j].append(i[0][j])
                else:
                    self.moveLine[j] = [i[0][j], ]
        print(self.moveLine)
        for i in self.moveLine:
            self.data.loc[i, '5 이평선'] = self.moveLine[i][1]
            self.data.loc[i, '10 이평선'] = self.moveLine[i][2]
            self.data.loc[i, '20 이평선'] = self.moveLine[i][3]

    def makeMoveLineForVolume(self, list):

        for i in list:
            for j in i[0].keys():
                if self.moveLineForVolume.__contains__(j):
                    self.moveLineForVolume[j].append(i[0][j])
                else:
                    self.moveLineForVolume[j] = [i[0][j], ]
        for i in self.moveLineForVolume:
            self.data.loc[i, '5 거래량'] = self.moveLineForVolume[i][0]
            self.data.loc[i, '10 거래량'] = self.moveLineForVolume[i][1]
            self.data.loc[i, '20 거래량'] = self.moveLineForVolume[i][2]

    def returnMoveLine(self):
        return self.moveLine


    def getRealData(self):
        # 종가 데이터
        ret = self.data['종가']
        ret.__setattr__("name", "real")
        return ret

    def makeMeanData(self, standard):
        # standard (5일,10일,20일 etc...)에 맞게 rolling 후 평균 산출
        mean = self.data['종가'].rolling(window=standard).mean()
        mean.__setattr__("name", str(standard))
        return mean

    def makeMeanVolume(self, standard):
        # standard (5일,10일,20일 etc...)에 맞게 rolling 후 평균 산출
        mean = self.data['거래량'].rolling(window=standard).mean()
        mean.__setattr__("name", str(standard))
        return mean

    def returnAllData(self):
        listDict = {}

        for i in self.list:
            for j in i[0].keys():
                if listDict.__contains__(j):
                    listDict[j].append(i[0][j])
                else:
                    listDict[j] = [i[0][j], ]

        return listDict
