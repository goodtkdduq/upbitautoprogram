# upbitAutoProgram

[ 프로그램 사용법 ]

**1. Login**
![캡처](/img/login.JPG)

- ID : 회원 가입을 통한 가입 필요
- PWD : 회원 가입을 통한 가입 필요
- ACCESE_KEY : Upbit API 사용 ACCESE_KEY (외부 공개 금지)
- SECRET_KEY : Upbit APi 사용 SECRET_KEY (외부 공개 금지)

**2. Logic 작성**
![캡처](/img/Logic_full.JPG)

1) 조건 설정
 - 탐색할 종목 선택
 - 탐색할 로직 작성
 - 봉 선택
 - AND 조건 설정

2) Logic 확인 창
 - 작성된 Logic 확인

3) Program Start하여 종목 별 Logic 조건 탐색
 - 조건 저장
 - 저장된 조건 가져오기
 - 선택 조건 삭제
 - 조건 검색 시작

4) 탐색된 Logic List Up
 - 탐색 시간, 탐색 종목, 발생 조건(2번에서 작성된 Logic) , 현재가
 - 동일 알람 미 발생
 - 알람 발생 시 등록된 Telegram ID로 메세지 전송



#
**2-1) 조건 설정**
#
 ![캡처](/img/logic_logic.JPG)
 #

  ![캡처](/img/logic_select.JPG)
  - [ ] 종목 LIST Button Click
  - [ ] 종목 선택 (기정된 Group도 선택 가능)
 
#
 - 탐색할 로직 작성 

  ![캡처](/img/logic_logic.JPG)
  - [ ] 첫번째, 두번째 항목 내 설정금액, 현재가, 이평선, 거래량 등을 선택
  - [ ] 선택된 항목끼리 등호를 선택하여 추가 Button 클릭

     ※ 봉선택 가능.

     ※ 첫번째, 두번째 항목 곱 연산 가능

     ※ 현재가, 이평선 등 선택 시 이전 봉 선택 가능

#
 - AND 조건 설정

  ![캡처](/img/logic_add.JPG)
  - [ ] 먼저 작성한 Logic과 AND 조건으로 묶고 싶을 시 "기존 LOGIC에 AND 조건으로 추가" 항목에서 기존 항목 선택

#
**2-2) Logic 확인 창**
#

 - 작성된 Logic 확인
 
![캡처](/img/logic_check.JPG)
  - [ ] 작성한 LOGIC 확인 가능
#  
![캡처](/img/logic_check2.JPG)
  - [ ] 위와 같이 AND 조건으로 묶인 LOGIC은 선행 번호가 같이 할당됨. (EX. 2-0번과 2-1번은 AND 조건으로 묶인 LOGIC)

#
**2-3) Logic 확인 창**
#
  - 조건 검색 시작

  ![캡처](/img/logic_start.JPG)
  - [ ] 조건 검색 시작 Button Click 시 Logic 동작
  ※ 선택한 종목만 탐색
  ※ and 조건인 Logic은 모든 Logic이 만족해야 Alert 발생
  ※ 발생한 Alert은 아래(2-4) 그리고 Telegram을 통해 전송 가능

#
**2-4) 탐색된 Logic List Up**
#
  - 발생된 Alert은 아래 창에 List Up

  ![캡처](/img/logic_catch.JPG)
  ※ 알람 시간 : Alert 발생 시간

  ※ 종목 : Alert 발생 종목

  ※ 발생 조건 : 작성된 Logic

  ※ 현재가 : Alert 발생 때의 현재가
  
#
   - 발생된 Alert은 Telegram으로도 전송가능

  ![캡처](/img/logic_tele.JPG)
  
  ※ 그래프 : Alert 발생 시 OHLC
  ※ TEXT : Alert 창과 동일



