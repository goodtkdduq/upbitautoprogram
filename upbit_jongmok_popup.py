import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5 import uic
import pyupbit
form_class = uic.loadUiType("UPBIT_JONGMOK_UI.ui")[0]
class UpbitJongmokList(QDialog,form_class):
    def __init__(self,list = None):
        super().__init__()
        self.setupUi(self)
        self.setWindowTitle('종목선택')
        if list!= None:
            for i in list:
                #self.tab1_upbit_jongmokList.addItem(i)
                self.tab1_group_add_user.addItem(i)
        tickers = pyupbit.get_tickers()
        for ticker in tickers:
            if 'KRW' in ticker:
                self.tab1_group_add_KRW.addItem(ticker)
            elif 'BTC' in ticker:
                self.tab1_group_add_BTC.addItem(ticker)
            else:
                self.tab1_group_add_USDT.addItem(ticker)

        self.tab1_group_add_KRW.itemDoubleClicked.connect(self.choose)

        self.tab1_group_add_BTC.itemDoubleClicked.connect(self.choose2)
        self.tab1_group_add_USDT.itemDoubleClicked.connect(self.choose3)
        self.tab1_group_add_user.itemDoubleClicked.connect(self.choose4)


    def choose(self):
        self.jongmok = self.tab1_group_add_KRW.item(self.tab1_group_add_KRW.currentRow()).text()
        self.close()
    def choose2(self):
        self.jongmok = self.tab1_group_add_BTC.item(self.tab1_group_add_BTC.currentRow()).text()
        self.close()
    def choose3(self):
        self.jongmok = self.tab1_group_add_USDT.item(self.tab1_group_add_USDT.currentRow()).text()
        self.close()
    def choose4(self):
        self.jongmok = self.tab1_group_add_user.item(self.tab1_group_add_user.currentRow()).text()
        self.close()