import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt
from PyQt5 import uic
from Condition import Condition
from MergeCondition import MergeCondition
import datetime
import threading
import time
import traceback
from telegram import Telegram
import os
import configparser
from upbit_jongmok_popup import UpbitJongmokList
from upbit import Upbit
import requests
from plyer import notification as noti
from GroupManager import GroupManager
import json
from mpl_finance import candlestick2_ohlc
import matplotlib.ticker as ticker
import matplotlib.pyplot as plt
from tickets import JongmokUpbit,BuyStatus
form_class = uic.loadUiType("UI.ui")[0]

class MyWindow(QMainWindow,form_class):
    version = '1.1.2'
    mergeLogics={}
    innerLogics={}
    mergeLogicsCnt=1
    alertStatus={}
    token = ''
    mc = ''
    tele = Telegram(token)
    logicSt=False
    server='172.30.1.27'
    port='5050'
    group={}
    bong = '3min'
    jongmokDict={}
    accese_key = "temp"
    secret_key = "temp"
    def versionCheck(self):
        res = requests.get('http://' + self.server + ':' + self.port + '/123/api/version')
        print(res.text)
        serverVersion = res.text
        print(os.getcwd())
        if serverVersion != self.version:
            os.startfile(os.getcwd()+'\\updater.exe')
            time.sleep(3)
            app.exit()
            return False
        else:
            print('version 같음')
            return True

    def configLoad(self):
        self.configPath ='./ConfigTelegram.config'
        if os.path.exists(self.configPath) == False:
            f = open(self.configPath,'w')
            f.close()
            print("config 파일 생성")
        try:
            config = configparser.ConfigParser()
            config.read(self.configPath)
            self.mc = config.get('Telegram', 'userId')
        except:
            print(traceback.format_exc())

        try:
            config = configparser.ConfigParser()
            config.read(self.configPath)
            self.token=config.get('Telegram','key')
            self.tab4_tele_label_key.setText(self.token)
            self.tele = Telegram(self.token,mc = self.mc)
        except:
            print(traceback.format_exc())


    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.setWindowTitle('Coin Master - '+self.version)
        self.configLoad()
        self.tab4_login_edit_id.setFocus()
        self.tabWidget.setTabEnabled(0, False)
        self.tabWidget.setTabEnabled(1, False)
        self.tabWidget.setTabEnabled(2, False)
        self.tabWidget.setTabEnabled(3, True)
        self.tabWidget.setTabEnabled(4, False)
        self.tabWidget.setTabEnabled(5, False)
        self.tab4_frame_join.setVisible(False)
        self.tab4_btn_join.setVisible(False)
        self.tab4_logout_btn.setVisible(False)
        self.frame_3.setVisible(False)
        self.tab1_btn_stop.setVisible(False)

        ### TAB1
        self.tab1_com_first.activated.connect(self.visibleSettingFirst)
        self.tab1_btn_addCondition.clicked.connect(self.startSearch)
        self.tab1_btn_addCondition_2.clicked.connect(self.startSearch2)
        self.tab1_btn_start.clicked.connect(self.startLogicThread)
        self.tab1_btn_jongmokList.clicked.connect(self.jongmokListPopup)
        self.tab1_btn_conditionDelete.clicked.connect(self.deleteSelectedLogic)
        self.tab1_btn_stop.clicked.connect(self.stopLogicThread)
        self.tab1_btn_groupList.clicked.connect(self.group_manager)
        self.tab1_btn_alert_init.clicked.connect(self.alert_init)
        self.tab1_radio_1min.clicked.connect(self.bongChanged)
        self.tab1_radio_3min.clicked.connect(self.bongChanged)
        self.tab1_radio_5min.clicked.connect(self.bongChanged)
        self.tab1_radio_10min.clicked.connect(self.bongChanged)
        self.tab1_radio_1day.clicked.connect(self.bongChanged)
        self.tab1_radio_7day.clicked.connect(self.bongChanged)
        self.tab1_btn_save_logic.clicked.connect(self.saveLogics)
        self.tab1_btn_get_logic.clicked.connect(self.getLogics)
        self.tab1_com_logics.currentTextChanged.connect(self.getLogic)
        #self.tab1_table_alert.currentCellChanged.connect(self.drawChart)


        self.tab1_bar_st.setStyleSheet("QProgressBar{\n"
                                       "       border: 2px solid grey;\n"
                                       "       border-radius: 5px;\n"
                                       "       text-align: center\n"
                                       "}\n"
                                       "\n"
                                       "QProgressBar::chunk {\n"
                                       "       \n"
                                       "    background-color: rgb(255, 46, 49);\n"
                                       "}")

        ## 로직 넣는 Table 크기
        self.tab1_table_condition.setColumnWidth(0, 40)
        self.tab1_table_condition.setColumnWidth(1, 80)
        self.tab1_table_condition.setColumnWidth(2, 80)
        self.tab1_table_condition.setColumnWidth(3, 40)
        self.tab1_table_condition.setColumnWidth(4, 30)
        self.tab1_table_condition.setColumnWidth(5, 80)
        self.tab1_table_condition.setColumnWidth(6, 40)
        self.tab1_table_condition.setColumnWidth(7, 80)
        self.tab1_table_condition.setColumnWidth(8, 40)
        self.tab1_table_condition.setColumnWidth(9, 40)
        self.tab1_table_condition.setColumnWidth(10, 40)
        self.tab1_table_condition.setColumnWidth(11, 130)
        ## 알람 Table 크기
        self.tab1_table_alert.setColumnWidth(0, 130)
        self.tab1_table_alert.setColumnWidth(1, 80)
        self.tab1_table_alert.setColumnWidth(2, 500)
        self.tab1_table_alert.setColumnWidth(3, 100)
        self.tab1_table_alert.setColumnWidth(4, 130)
        self.tab1_spin_bong_first.setVisible(False)
        self.tab1_label_bong_first.setVisible(False)

        ### TAB 4
        self.tab4_tele_check.clicked.connect(self.changeTelegramCheckBox)
        self.tab4_tele_btn_key.clicked.connect(self.saveTeleKey)
        self.tab4_login_btn_2.clicked.connect(self.join)
        self.tab4_check_agree.clicked.connect(self.agree)
        self.tab4_btn_join_cencel.clicked.connect(self.join_cencel)
        self.tab4_btn_join.clicked.connect(self.join_detail)
        self.tab4_logout_btn.clicked.connect(self.logout)
        #login
        self.tab4_login_btn.clicked.connect(self.login)
        self.tab4_login_edit_pwd.returnPressed.connect(self.login)

    ## TAB1 로직
    def drawChart(self,data):
        try:
            # 차트 그리기
            data = data.tail(30)
            fig = plt.figure(figsize=(20, 10))
            ax = fig.add_subplot(211)
            ax2 = fig.add_subplot(212)
            index = data.index.astype('str')  # 캔들스틱 x축이 str로 들어감
            ax.plot(index,data['5 이평선'],label = '5',linewidth=0.7)
            ax.plot(index, data['10 이평선'], label='10', linewidth=0.7)
            ax.plot(index, data['20 이평선'], label='20', linewidth=0.7)
            ax2.bar(index,data['거래량'])
            ax2.plot(index,data['5 거래량'],label = 'vol 5',linewidth=0.7)
            ax2.plot(index, data['10 거래량'], label='vol 10', linewidth=0.7)
            ax2.plot(index, data['20 거래량'], label='vol 20', linewidth=0.7)

            ax.xaxis.set_major_locator(ticker.MaxNLocator(3))
            ax.yaxis.set_major_locator(ticker.MaxNLocator(3))
            ax2.xaxis.set_major_locator(ticker.MaxNLocator(3))
            ax2.yaxis.set_major_locator(ticker.MaxNLocator(3))

            candlestick2_ohlc(ax,data['시가'],data['고가'],data['저가'],data['종가'],width=0.5,colorup='r',colordown='b')
            ax.legend()
            plt.grid()
            plt.savefig('./img.png')
            #plt.show()
            self.tele.sendPhoto('./img.png')
        except:
            print(traceback.format_exc())
    def getLogic(self):
        try:
            msgRes = QMessageBox.warning(self.tabWidget, '경고', '작성된 Logic이 모두 삭제 됩니다.', QMessageBox.Ok | QMessageBox.No)
            if msgRes == QMessageBox.No:
                return;

            self.mergeLogics.clear()
            self.innerLogics.clear()
            delList =[]
            for i in range(self.tab1_table_condition.rowCount()):
                delList.append(self.tab1_table_condition.item(i, 0).text())
            for i in delList:
                try:
                    print("removeItem",i)
                    self.tab1_com_first.removeItem(self.tab1_com_first.findText(i))
                    self.tab1_com_second.removeItem(self.tab1_com_second.findText(i))
                except:
                    print(traceback.format_exc())
            self.tab1_table_condition.setRowCount(0)
            text = self.tab1_com_logics.currentText()
            res = requests.get('http://' + self.server + ':' + self.port + '/123/api/' + self.id + '/logicGet/'+text)
            print(res.text)
            data = json.loads(res.text)
            print(data)
            logicName = list(data.keys())[0]

            for logicNum in data[logicName].keys():
                print(logicNum)
                print(data[logicName][logicNum][0])
                con = self.makeConditon(data[logicName][logicNum][0],
                                  data[logicName][logicNum][1],
                                  data[logicName][logicNum][2],
                                  data[logicName][logicNum][3],
                                  data[logicName][logicNum][4],
                                  data[logicName][logicNum][5],
                                  data[logicName][logicNum][6],
                                  data[logicName][logicNum][7],
                                  data[logicName][logicNum][8],
                                  data[logicName][logicNum][9],
                                  data[logicName][logicNum][10])
                sp = logicNum.replace('번','').split('-')
                key = int(sp[0])
                index = int(sp[1])
                if index == 0:
                    self.tab1_com_mergeLogic.addItem(str(key) + '번')
                    self.mergeLogics[key] = {}
                    self.mergeLogics[key][index] = con
                    print("mergelogics", self.mergeLogics)
                    self.mergeLogicsCnt =key+1
                else:
                    self.mergeLogics[key][index] = con

                self.tab1_table_condition.setRowCount(self.tab1_table_condition.rowCount() + 1)
                self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 0,
                                                  QTableWidgetItem(logicNum))
                curTime = datetime.datetime.now()
                self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 1,
                                                  QTableWidgetItem(data[logicName][logicNum][0]))

                item = QTableWidgetItem(data[logicName][logicNum][1])
                item.setTextAlignment(Qt.AlignRight | Qt.AlignVCenter)
                self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 2, item)

                self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 3,
                                                  QTableWidgetItem(str(data[logicName][logicNum][5])))

                if data[logicName][logicNum][3] == '0':
                    big = '>'
                elif data[logicName][logicNum][3] == '1':
                    big = '<'
                elif data[logicName][logicNum][3] == '2':
                    big = '+'
                elif data[logicName][logicNum][3] == '3':
                    big = '-'
                elif data[logicName][logicNum][3] == '4':
                    big = '*'
                elif data[logicName][logicNum][3] == '5':
                    big = '/'

                item = QTableWidgetItem(big)
                item.setTextAlignment(Qt.AlignCenter | Qt.AlignVCenter)
                self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 4, item)
                item = QTableWidgetItem(data[logicName][logicNum][2])
                item.setTextAlignment(Qt.AlignRight | Qt.AlignVCenter)
                self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 5, item)
                self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 6,
                                                  QTableWidgetItem(str(data[logicName][logicNum][6])))
                item = QTableWidgetItem(data[logicName][logicNum][7])
                item.setTextAlignment(Qt.AlignCenter | Qt.AlignVCenter)
                self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 7, item)

                self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 8,
                                                  QTableWidgetItem(data[logicName][logicNum][8]))
                self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 9,
                                                  QTableWidgetItem(data[logicName][logicNum][9]))

                item = QTableWidgetItem(data[logicName][logicNum][4])
                item.setTextAlignment(Qt.AlignCenter | Qt.AlignVCenter)
                self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 10, item)
                self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 11,
                                                  QTableWidgetItem(curTime.strftime('%Y-%m-%d %H:%M:%S')))

                if big == '+' or big=='-' or big == '*' or big == '/':
                    self.tab1_com_first.addItem(logicNum)
                    self.tab1_com_second.addItem(logicNum)
            print(self.mergeLogics)
            print(self.innerLogics)
        except:
            print(traceback.format_exc())


    def getLogics(self):
        try:
            self.tab1_com_logics.clear()
            res = requests.get('http://' + self.server + ':' + self.port + '/123/api/' + self.id + '/logicGet')
            print(res.text)
            items = res.text.split('|')
            items = items[:-1]
            for item in items:
                print(item)
                self.tab1_com_logics.addItem(item)
        except:
            print(traceback.format_exc())


    def saveLogics(self):
        try:
            logicName = self.tab1_line_logicName.text()
            if logicName == '':
                msg = QMessageBox()
                msg.setWindowFlags(Qt.FramelessWindowHint)
                msg.warning(self.tabWidget, 'Error', 'Logic 이름을 적어주세요..', QMessageBox.Ok)
                return

            print(self.mergeLogics)
            ret ={}
            ret[logicName] ={}
            for logics in self.mergeLogics:
                for logic in self.mergeLogics[logics]:
                    ret[logicName][self.mergeLogics[logics][logic].logicNumber]=self.mergeLogics[logics][logic].returnCon()

            print(ret)
            res = requests.post('http://' + self.server + ':' + self.port + '/123/api/' + self.id + '/logicUpdate',
                                json=ret)
        except Exception as e:
            print(traceback.format_exc())

    def bongChanged(self):
        try:
            print(self.bong)
            if self.tab1_radio_1min.isChecked():
                self.bong = '1min'
            elif self.tab1_radio_3min.isChecked():
                self.bong = '3min'
            elif self.tab1_radio_5min.isChecked():
                self.bong = '5min'
            elif self.tab1_radio_10min.isChecked():
                self.bong = '10min'
            elif self.tab1_radio_1day.isChecked():
                self.bong = '1day'
            elif self.tab1_radio_7day.isChecked():
                self.bong = 'week'
            print(self.bong)
        except:
            print(traceback.format_exc())

    def alert_init(self):
        try:
            self.alertStatus.clear()
            self.tab1_table_alert.setRowCount(0)
        except Exception as e:
            print(traceback.format_exc())
    def group_manager(self):
        try:
            dlg = GroupManager(groups=self.group)
            dlg.exec_()
            self.group = dlg.group
            print(self.group)
            res = requests.post('http://' + self.server + ':' + self.port + '/123/api/'+self.id+'/groupUpdate',json=self.group)
        except:
            print(traceback.format_exc())

    def startLogic(self):
        while True:
            try:
                if self.logicSt==False:
                    break
                for key in self.mergeLogics.keys():
                    if key in self.alertStatus.keys() and self.tab1_checkTime.isChecked():
                        continue
                    first = list(self.mergeLogics[key].keys())[0]
                    if 'GROUP' in self.mergeLogics[key][first].jongmok and self.mergeLogics[key][first].jongmok in self.group.keys():
                        for jongmok in self.group[self.mergeLogics[key][first].jongmok]:
                            if (key,jongmok) in self.alertStatus.keys() and self.tab1_checkTime.isChecked():
                                continue
                            before = True
                            for logic in self.mergeLogics[key]:
                                logic = self.mergeLogics[key][logic]
                                upbit = Upbit(jongmok, self.bong)
                                info = upbit.getData()
                                df = info[0]
                                now = info[1]
                                merge = MergeCondition()
                                item = logic.analysis(now, df)
                                result = False
                                print(logic.logicNumber)
                                # 결과값이 float인 것은 해당 logic을 사용하는 logic에 결과값 전달
                                if str(type(item[0])) == "<class 'float'>":
                                    logicList = self.innerLogics[logic.logicNumber]
                                    for lo in logicList.keys():
                                        if logic.logicNumber == logicList[lo].first:
                                            logicList[lo].setFirstCondition(item[0])
                                        if logic.logicNumber == logicList[lo].second:
                                            logicList[lo].setSecondCondition(item[0])
                                else:
                                    result = merge.check(before, item[0], 'and')
                                    before = item[0]


                            if logic.jongmok not in self.jongmokDict.keys():
                                self.jongmokDict[logic.jongmok] = JongmokUpbit(logic.jongmok, self.accese_key,self.secret_key)
                            print("result : ", result)
                            if result == True:
                                if logic.action == "매수" and (self.jongmokDict[logic.jongmok].st == BuyStatus().sell):
                                    self.jongmokDict[logic.jongmok].buy(time=5, limit=10)
                                    res = self.alert(self.mergeLogics[key], now, key, jong=jongmok)
                                    self.tele.sendMessage(res[0])
                                    self.alertStatus[(key, jongmok)] = res[1]
                                if logic.action == "매도" and (self.jongmokDict[logic.jongmok].st == BuyStatus().buy):
                                    self.jongmokDict[logic.jongmok].sell()
                                    res = self.alert(self.mergeLogics[key], now, key, jong=jongmok)
                                    self.tele.sendMessage(res[0])
                                    self.alertStatus[(key, jongmok)] = res[1]
                            time.sleep(1)
                    else:
                        before = True
                        for logic in self.mergeLogics[key]:
                            logic = self.mergeLogics[key][logic]
                            upbit = Upbit(logic.jongmok,self.bong)
                            info = upbit.getData()
                            df = info[0]
                            now = info[1]
                            print("inner:",logic.analysis(now, df))
                            merge = MergeCondition()
                            item = logic.analysis(now, df)
                            result = False
                            print(logic.logicNumber)
                            # 결과값이 float인 것은 해당 logic을 사용하는 logic에 결과값 전달
                            if str(type(item[0])) == "<class 'float'>":
                                logicList = self.innerLogics[logic.logicNumber]
                                for lo in logicList.keys():
                                    if logic.logicNumber == logicList[lo].first:
                                        logicList[lo].setFirstCondition(item[0])
                                    if logic.logicNumber == logicList[lo].second:
                                        logicList[lo].setSecondCondition(item[0])
                            else:
                                result = merge.check(before,item[0],'and')
                                before = item[0]

                        print("result : ",result)
                        if logic.jongmok not in self.jongmokDict.keys():
                            self.jongmokDict[logic.jongmok] = JongmokUpbit(logic.jongmok,self.accese_key,self.secret_key)
                        if result == True:
                            if logic.action == "매수" and (self.jongmokDict[logic.jongmok].st == BuyStatus().sell):
                                self.jongmokDict[logic.jongmok].buy(time = 5, limit= 10)
                                res = self.alert(self.mergeLogics[key], now, key)
                                self.tele.sendMessage(res[0])
                                self.drawChart(upbit.df)
                                self.alertStatus[key] = res[1]
                            if logic.action == "매도" and (self.jongmokDict[logic.jongmok].st == BuyStatus().buy):
                                self.jongmokDict[logic.jongmok].sell()
                                res = self.alert(self.mergeLogics[key], now, key)
                                self.tele.sendMessage(res[0])
                                self.drawChart(upbit.df)
                                self.alertStatus[key] = res[1]
                        time.sleep(1)
                time.sleep(5)
            except Exception:
                print(traceback.format_exc())


    def alert(self,logics,nowPrice,key,jong=None):
        try:
            print("alert 발생")
            self.tab1_table_alert.setRowCount(self.tab1_table_alert.rowCount() + 1)
            tm = datetime.datetime.now()
            ocurTime = tm.strftime('%Y-%m-%d %H:%M:%S')
            if jong==None:
                first = list(logics.keys())[0]
                jongmok = logics[first].jongmok
            else:
                jongmok = jong
            logicNum = str(key) + '번'
            self.tab1_table_alert.setItem(self.tab1_table_alert.rowCount() - 1, 0, QTableWidgetItem(ocurTime))
            self.tab1_table_alert.setItem(self.tab1_table_alert.rowCount() - 1, 1,QTableWidgetItem(jongmok))
            desc =''
            cnt=0
            for logic in logics:
                logic = logics[logic]
                desc = desc+ logic.description+'\n'
                cnt+=23
            desc = desc[:-1]

            self.tab1_table_alert.setItem(self.tab1_table_alert.rowCount() - 1, 2, QTableWidgetItem(desc))
            self.tab1_table_alert.setItem(self.tab1_table_alert.rowCount() - 1, 3, QTableWidgetItem(str(nowPrice)))
            self.tab1_table_alert.setItem(self.tab1_table_alert.rowCount() - 1, 4, QTableWidgetItem(logicNum))
            time.sleep(0.1)
            self.tab1_table_alert.setRowHeight(self.tab1_table_alert.rowCount() - 1, cnt)
            self.tab1_table_alert.scrollToBottom()
            self.tab1_table_alert.setCurrentCell(self.tab1_table_alert.rowCount()-1,0)

            msg = '발생 시간 : '+ ocurTime+'\n'+\
                  '종목 : '+ jongmok+ '\n'+\
                  'Action : '+logic.action+'\n'+\
                  '[발생조건]\n'+desc+'\n현재가 : '+str(nowPrice)+'\n'+'로직번호 : '+logicNum


            return msg, ocurTime
        except:
            print(traceback.format_exc())



    def startLogicThread(self):
        if self.tab1_table_condition.rowCount() == 0:
            noti.notify(title='Auto ZooZoo',
                        message='설정된 Logic이 없습니다.\n로직 설정 후 다시 시도하세요.', timeout=1)
            QMessageBox.warning(self.tabWidget, 'Error',
                                '설정된 Logic이 없습니다.\n로직 설정 후 다시 시도하세요.', QMessageBox.Ok)
            return
        self.tab1_btn_stop.setVisible(True)
        noti.notify(title='Auto ZooZoo',message='로직이 동작중입니다.',timeout=1)
        self.logicSt=True
        self.thread = threading.Thread(target=self.startLogic)
        self.thread.daemon=True
        self.thread.start()

        self.tab1_bar_st.setStyleSheet("QProgressBar{\n"
                                       "       border: 2px solid grey;\n"
                                       "       border-radius: 5px;\n"
                                       "       text-align: center\n"
                                       "}\n"
                                       "\n"
                                       "QProgressBar::chunk {\n"
                                       "       \n"
                                       "    background-color: rgb(91, 214, 34);\n"
                                       "}")

    def stopLogicThread(self):
        try:
            self.tab1_btn_stop.setVisible(False)
            self.logicSt = False
            noti.notify(title='Auto ZooZoo', message='로직이 동작이 중지되었습니다.', timeout=1)
            self.tab1_bar_st.setStyleSheet("QProgressBar{\n"
                                           "       border: 2px solid grey;\n"
                                           "       border-radius: 5px;\n"
                                           "       text-align: center\n"
                                           "}\n"
                                           "\n"
                                           "QProgressBar::chunk {\n"
                                           "       \n"
                                           "    background-color: rgb(255, 46, 49);\n"
                                           "}")

        except Exception:
            print(traceback.format_exc())

    def visibleSettingFirst(self,str):
        if str == 0 :
            self.tab1_line_first.setVisible(True)
            self.tab1_spin_bong_first.setVisible(False)
            self.tab1_label_bong_first.setVisible(False)
        else:
            self.tab1_line_first.setVisible(False)
            self.tab1_spin_bong_first.setVisible(True)
            self.tab1_label_bong_first.setVisible(True)


    def settingLogic(self,cnt = 1,minerCnt=0):
        try:
            jongmok = self.tab1_line_jongmok.text()
            if jongmok == "":
                msg = QMessageBox()
                msg.setWindowFlags(Qt.FramelessWindowHint)
                msg.warning(self.tabWidget, 'Error', '종목을 선정해주세요.', QMessageBox.Ok)

                return
            firstMul = float(self.tab1_spin_first.text())
            secondMul = float(self.tab1_spin_second.text())
            firstBefor = int(self.tab1_spin_bong_first.text())
            secondBefor = int(self.tab1_spin_bong_second.text())
            logicNumber=str(cnt)+'-'+str(minerCnt)+'번'
            print(firstMul, secondMul)


            con1 = self.tab1_com_first.currentText()
            if con1 == '설정금액':
                if self.tab1_line_first.text() == '':
                    print("설정해야되")
                    msg = QMessageBox()
                    msg.setText("설정금액을 입력해주세요.")
                    msg.exec_()
                    return
                else:
                    setting = self.tab1_line_first.text()
            else:
                setting = '-'
            con2 = self.tab1_com_second.currentText()



            if self.tab1_radio_buy.isChecked():
                action = '매수'
            else:
                action = '매도'
            self.tab1_table_condition.setRowCount(self.tab1_table_condition.rowCount() + 1)
            self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 0, QTableWidgetItem(logicNumber))
            curTime = datetime.datetime.now()
            self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 1, QTableWidgetItem(jongmok))
            item = QTableWidgetItem(con1)
            item.setTextAlignment(Qt.AlignRight | Qt.AlignVCenter)
            self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 2, item)
            self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 3, QTableWidgetItem(str(firstMul)))
            item = QTableWidgetItem(self.tab1_com_compare.currentText())
            item.setTextAlignment(Qt.AlignCenter | Qt.AlignVCenter)
            self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 4, item)
            item = QTableWidgetItem(con2)
            item.setTextAlignment(Qt.AlignRight | Qt.AlignVCenter)
            self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 5, item)
            self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 6, QTableWidgetItem(str(secondMul)))
            item = QTableWidgetItem(setting)
            item.setTextAlignment(Qt.AlignCenter | Qt.AlignVCenter)
            self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 7, item)

            self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 8, QTableWidgetItem(str(firstBefor)))
            self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 9,  QTableWidgetItem(str(secondBefor)))

            item = QTableWidgetItem(action)
            item.setTextAlignment(Qt.AlignCenter | Qt.AlignVCenter)
            self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 10, item)
            self.tab1_table_condition.setItem(self.tab1_table_condition.rowCount() - 1, 11,
                                              QTableWidgetItem(curTime.strftime('%Y-%m-%d %H:%M:%S')))

            print(con1, con2, setting)
            ##
            '''
            df = pyupbit.get_ohlcv('KRW-ARK', interval="minute3")
            now = pyupbit.get_current_price('KRW-ARK')
            df.rename(columns={'open': '시가'}, inplace=True)
            df.rename(columns={'high': '고가'}, inplace=True)
            df.rename(columns={'low': '저가'}, inplace=True)
            df.rename(columns={'close': '종가'}, inplace=True)
            df.rename(columns={'volume': '거래량'}, inplace=True)
            info = MeanMoveLine(data=df, useVolume=True)
            '''
            '''
            upbit = Upbit(jongmok, '3min')
            info = upbit.getData()
            df = info[0]
            now = info[1]
            '''
            if self.tab1_com_compare.currentText() == '>':
                bigger = 0
            elif self.tab1_com_compare.currentText() == '<':
                bigger = 1
            elif self.tab1_com_compare.currentText() == '+':
                bigger = 2
            elif self.tab1_com_compare.currentText() == '-':
                bigger = 3
            elif self.tab1_com_compare.currentText() == '*':
                bigger = 4
            elif self.tab1_com_compare.currentText() == '/':
                bigger = 5

            con =self.makeConditon(jongmok,con1, con2, bigger, action, firstMul, secondMul, setting, firstBefor, secondBefor,logicNumber)
            '''
            con = Condition(jongmok,con1, con2, bigger, action, firstMul, secondMul, setting, firstBefor, secondBefor,logicNumber)
            #item = con.analysis(now, df)

            '''
            '''
            if bigger ==2 or bigger==3:
                self.tab1_com_first.addItem(logicNumber)
                self.tab1_com_second.addItem(logicNumber)
            if '번' in con1:     #logic 이름에 '번'이 들어가있으면
                if con1 in self.innerLogics.keys():
                    self.innerLogics[con1][len(self.innerLogics[con1])] = con
                else:
                    #self.innerLogics[con1]= [con,]
                    self.innerLogics[con1] = {}
                    self.innerLogics[con1][len(self.innerLogics[con1])]=con

            if '번' in con2:     #logic 이름에 '번'이 들어가있으면
                if con2 in self.innerLogics.keys():
                    self.innerLogics[con2][len(self.innerLogics[con2])] = con
                else:
                    #self.innerLogics[con1]= [con,]
                    self.innerLogics[con2] = {}
                    self.innerLogics[con2][len(self.innerLogics[con2])]=con
            '''
            return con
        except Exception as e:
            print(traceback.format_exc())
        return con

    def makeConditon(self,jongmok, con1, con2, bigger, action, firstMul, secondMul, setting, firstBefor, secondBefor,logicNumber):
        con = Condition(jongmok, con1, con2, bigger, action, firstMul, secondMul, setting, firstBefor, secondBefor,logicNumber)
        # item = con.analysis(now, df)

        '''
        if str(type(item[0])) == "<class 'float'>":
            self.tab1_com_first.addItem(logicNumber)
            self.tab1_com_second.addItem(logicNumber)
        '''
        if bigger == 2 or bigger == 3 or bigger==4 or bigger==5:
            self.tab1_com_first.addItem(logicNumber)
            self.tab1_com_second.addItem(logicNumber)
        if '번' in con1:  # logic 이름에 '번'이 들어가있으면
            if con1 in self.innerLogics.keys():
                self.innerLogics[con1][len(self.innerLogics[con1])] = con
            else:
                # self.innerLogics[con1]= [con,]
                self.innerLogics[con1] = {}
                self.innerLogics[con1][len(self.innerLogics[con1])] = con

        if '번' in con2:  # logic 이름에 '번'이 들어가있으면
            if con2 in self.innerLogics.keys():
                self.innerLogics[con2][len(self.innerLogics[con2])] = con
            else:
                # self.innerLogics[con1]= [con,]
                self.innerLogics[con2] = {}
                self.innerLogics[con2][len(self.innerLogics[con2])] = con

        return con

    def startSearch(self):
        try:
            jongmok = self.tab1_line_jongmok.text()
            if jongmok == "":
                msg = QMessageBox()
                msg.setWindowFlags(Qt.FramelessWindowHint)
                msg.warning(self.tabWidget, 'Error', '종목을 선정해주세요.', QMessageBox.Ok)
                return
            if self.tab1_line_first.text() == '' and self.tab1_com_first.currentText=='설정금액':
                print("설정해야되")
                msg = QMessageBox()
                msg.setText("설정금액을 입력해주세요.")
                msg.exec_()
                return
            con = self.settingLogic(self.mergeLogicsCnt,0)
            self.tab1_com_mergeLogic.addItem(str(self.mergeLogicsCnt)+'번')
            self.mergeLogics[self.mergeLogicsCnt]={}
            self.mergeLogics[self.mergeLogicsCnt][len(self.mergeLogics[self.mergeLogicsCnt])]=con
            print(self.mergeLogicsCnt)
            print("mergelogics",self.mergeLogics)
            self.mergeLogicsCnt+=1
        except:
            print(traceback.format_exc())

    def startSearch2(self):
        try:
            jongmok = self.tab1_line_jongmok.text()
            if jongmok == "":
                msg = QMessageBox()
                msg.setWindowFlags(Qt.FramelessWindowHint)
                msg.warning(self.tabWidget, 'Error', '종목을 선정해주세요.', QMessageBox.Ok)
                return

            logicNum = self.tab1_com_mergeLogic.currentText()
            logicNum=int(logicNum.replace('번',''))
            lenNum = self.checkLogicNumber(logicNum, len(self.mergeLogics[logicNum].keys()))
            con = self.settingLogic(logicNum,lenNum)
            print(logicNum)
            #self.mergeLogics[logicNum].append(con)

            self.mergeLogics[logicNum][lenNum]=con
            print("mergelogics", self.mergeLogics)
            print(self.innerLogics)
        except Exception as e:
            print(traceback.format_exc())

    def checkLogicNumber(self,logicNum,len):
        if len in self.mergeLogics[logicNum].keys():
            len = self.checkLogicNumber(logicNum,len+1)
        return len
    def jongmokListPopup(self):
        try:
            dlg = UpbitJongmokList(list = self.group.keys())
            dlg.exec_()
            self.tab1_line_jongmok.setText(dlg.jongmok)
        except:
            print(traceback.format_exc())

    def deleteSelectedLogic(self):
        print("---delete시작--")
        print(len(self.mergeLogics))
        print(self.mergeLogics.keys())
        print(len(self.innerLogics))
        print(self.innerLogics.keys())
        print("-----")
        try:
            row = self.tab1_table_condition.selectedIndexes()[0].row()
            allValue = self.tab1_table_condition.item(row,0).text()
            self.deleteLogic(allValue,row)
        except:
            print(traceback.format_exc())
        print("---delete결과--")
        print(len(self.mergeLogics))
        print(self.mergeLogics)
        print(len(self.innerLogics))
        print(self.innerLogics)

        print("-----")
    def deleteLogic(self,allValue,row):
        try:
            print("allValue",allValue)
            value = allValue.replace('번','')
            key = int(value.split('-')[0])
            index = int(value.split('-')[1])
            print("key",key)
            print("index",index)

            try:
                print(self.innerLogics.keys())
                for i in self.innerLogics[allValue].keys():
                    self.deleteLogic(self.innerLogics[allValue][i].logicNumber,self.findTableRow(self.innerLogics[allValue][i].logicNumber))
            except:
                print(traceback.format_exc())

            try:
                self.mergeLogics[key].__delitem__(index)
                print(self.mergeLogics[key])
                print("KEY 삭제",key)
                if len(self.mergeLogics[key].keys()) == 0:
                    self.mergeLogics.__delitem__(key)
                    self.tab1_com_mergeLogic.removeItem(self.tab1_com_mergeLogic.findText(str(key) + '번'))
            except :
                print(traceback.format_exc())

            try:
                self.innerLogics.__delitem__(allValue)
            except :
                print(traceback.format_exc())


            self.tab1_table_condition.removeRow(row)
            self.tab1_com_first.removeItem(self.tab1_com_first.findText(allValue))
            self.tab1_com_second.removeItem(self.tab1_com_second.findText(allValue))


        except:
            print(traceback.format_exc())

    def findTableRow(self,findLogicNumber):
        for i in range(self.tab1_table_condition.rowCount()):
            if self.tab1_table_condition.item(i,0).text() == findLogicNumber:
                return i

    ## TAB4 LOGIC
    def join(self):
        self.tab4_frame_join.setVisible(True)
    def agree(self):
        if self.tab4_check_agree.isChecked():
            self.tab4_btn_join.setVisible(True)
        else:
            self.tab4_btn_join.setVisible(False)

    def join_cencel(self):
        self.tab4_frame_join.setVisible(False)

    def join_detail(self):
        try:
            id = self.tab4_edit_id.text()
            pwd = self.tab4_edit_pwd.text()
            pwdCheck = self.tab4_edit_pwdCheck.text()
            name = self.tab4_edit_name.text()
            num = self.tab4_edit_num.text()

            if pwd == pwdCheck:
                data = {}
                data['id']=id
                data['pwd']=pwd
                data['name']=name
                data['num']=num
                res = requests.post('http://' + self.server + ':' + self.port + '/123/api/join',
                                    json=data)
                if res.text=='SAME':
                    QMessageBox.warning(self.tabWidget,'Error','중복된 ID 입니다.',QMessageBox.Ok)
                elif res.text=='OK':
                    QMessageBox.information(self.tabWidget, '완료', '회원가입 완료되었습니다.\n사용시간은 관리자에게 문의하세요.', QMessageBox.Ok)
                    self.join_cencel()
        except:
            print(traceback.format_exc())

    def changeTelegramCheckBox(self):
        st = self.tab4_tele_check.isChecked()
        if st == True:
            self.tab4_tele_edit_key.setEnabled(True)
            self.tab4_tele_edit_id.setEnabled(True)
            self.tab4_tele_btn_key.setEnabled(True)
            self.tab4_tele_btn_id.setEnabled(True)
        else:
            self.tab4_tele_edit_key.setEnabled(False)
            self.tab4_tele_edit_id.setEnabled(False)
            self.tab4_tele_btn_key.setEnabled(False)
            self.tab4_tele_btn_id.setEnabled(False)

    def saveTeleKey(self):
        try:

            res= self.tele.getMessage()
            if res == 'error':
                QMessageBox.warning(self.tabWidget,'error','텔레그램 채팅창에 메시지(\'hi\')를 입력후 다시 저장해주세요',QMessageBox.Ok)
            else:
                self.mc = res
                self.token = self.tab4_tele_edit_key.text()
                self.tab4_tele_label_key.setText(self.token)
                self.tele = Telegram(self.token)
                config = configparser.ConfigParser()
                config.write(self.configPath)
                config.add_section('Telegram')
                config.set('Telegram','key',self.token)
                config.set('Telegram', 'userId', str(self.mc))
                with open(self.configPath, "w") as fp:
                    config.write(fp)

        except:
            print(traceback.format_exc())

    def saveTeleId(self):
        try:
            self.mc = self.tab4_tele_edit_id.text()
            self.tab4_tele_label_id.setText(self.mc)
            self.tele = Telegram(self.token)
            config = configparser.ConfigParser()
            config.write(self.configPath)
            config.add_section('Telegram')
            config.set('Telegram','userId',self.mc)
            config.set('Telegram', 'key', self.token)
            with open(self.configPath, "w") as fp:
                config.write(fp)

        except:
            print(traceback.format_exc())

    def logout(self):
        self.tabWidget.setTabEnabled(0, False)
        self.tabWidget.setTabEnabled(1, False)
        self.tabWidget.setTabEnabled(2, False)
        self.tabWidget.setTabEnabled(4, False)
        self.tabWidget.setTabEnabled(5, False)
        self.tab4_info_label.setText('로그아웃에 성공하였습니다.')
        self.tab4_logout_btn.setVisible(False)
        self.frame_3.setVisible(False)

    def login(self):
        try:
            self.id = self.tab4_login_edit_id.text()
            self.pwd = self. tab4_login_edit_pwd.text()
            print(self.id,self.pwd)
            res = requests.get('http://'+self.server+':'+self.port+'/123/api/login/'+self.id+'/'+self.pwd)
            if res.text == 'OK':
                self.tabWidget.setTabEnabled(0, True)
                self.tabWidget.setTabEnabled(1, True)
                self.tabWidget.setTabEnabled(2, True)
                self.tabWidget.setTabEnabled(3, True)
                self.tabWidget.setTabEnabled(4, True)
                self.tabWidget.setTabEnabled(5, True)
                self.tab4_info_label.setText('로그인에 성공하였습니다. '+self.id +'님, 환영합니다. ')
                resGroup = requests.get('http://' + self.server + ':' + self.port + '/123/api/' + self.id + '/groupGet')
                self.group = json.loads(resGroup.text)
                noti.notify(title='Auto ZooZoo', message=self.id + '님, 환영합니다. Auto ZooZoo에 Login 되었습니다.', timeout=1)
                self.tab4_logout_btn.setVisible(True)
                self.tab4_login_edit_id.setText('')
                self.tab4_login_edit_pwd.setText('')
                self.frame_3.setVisible(True)
            elif res.text == 'NoAuth':
                self.tab4_info_label.setText("이용기간이 만료되었습니다.\n사용기간 신청 : https://cafe.naver.com/upbit1 -> 프로그램 기간 신청")
                noti.notify(title='Auto ZooZoo', message='이용기간이 만료되었습니다.\n사용기간 신청 : https://cafe.naver.com/upbit1 -> 프로그램 기간 신청', timeout=1)
                QMessageBox.warning(self.tabWidget, 'Error', '이용기간이 만료되었습니다.\n사용기간 신청 : https://cafe.naver.com/upbit1 -> 프로그램 기간 신청', QMessageBox.Ok)

            else:
                self.tab4_info_label.setText("로그인에 실패했습니다. ID/PWD를 확인해주세요.")
                noti.notify(title='Auto ZooZoo', message='로그인에 실패했습니다. ID/PWD를 확인해주세요.', timeout=1)
                QMessageBox.warning(self.tabWidget, 'Error', '로그인에 실패했습니다. ID/PWD를 확인해주세요.', QMessageBox.Ok)
                
        except:
            print(traceback.format_exc())

if __name__ == "__main__":
    version = '1.0.0'
    app = QApplication(sys.argv)
    import os
    os.environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"
    app.setStyle("Fusion")
    myWindow=MyWindow()
    st = myWindow.versionCheck()
    if st == True:
        myWindow.show()
        app.exec_()
        print("실행완료")