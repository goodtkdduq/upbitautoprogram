from define import Define
import traceback
class Condition:
    define =Define()
    time=0
    def __init__(self,jongmok,first,second,biggerFirst,action,firstMultiply,secondMultiply,setting,firstBefor,secondBefor,logicNumber):
        try:
            self.condition1 = -1205
            self.condition2 = -1205
            self.jongmok = jongmok
            self.first = first
            self.second = second
            self.biggerFirst=int(biggerFirst)
            self.action = action
            self.firstMultiply = float(firstMultiply)
            self.secondMultiply = float(secondMultiply)
            self.setting = setting
            self.firstBefor = int(firstBefor)
            self.secondBefor = int(secondBefor)
            self.logicNumber= logicNumber
            if self.biggerFirst==0:
                big ='>'
            elif self.biggerFirst==1:
                big='<'
            elif self.biggerFirst==2:
                big='+'
            elif self.biggerFirst==3:
                big='-'
            elif self.biggerFirst ==4:
                big='*'
            elif self.biggerFirst == 5:
                big='/'
            self.description = str(self.first)+' '+big+' '+str(self.second)
        except:
            print(traceback.format_exc())
    def returnCon(self):
        return [self.condition1,self.condition2,self.jongmok,self.first,self.second,self.biggerFirst,self.action,self.firstMultiply,self.secondMultiply,self.setting,self.firstBefor,self.secondBefor,self.logicNumber]

    def classification(self,now,df):
        if self.define.현재가인가(self.first):
            self.condition1 = now
        elif self.define.설정금액인가(self.first):
            self.condition1 = self.setting
        elif self.define.매수가인가(self.first):
            self.condition1 = now
        elif '번' in self.first:
            pass
        else:
            self.condition1 = df[self.first][-1+self.firstBefor]

        if self.define.현재가인가(self.second):
            self.condition2 = now
        elif self.define.설정금액인가(self.second):
            self.condition2 = self.setting
        elif self.define.매수가인가(self.second):
            self.condition2 = now
        elif '번' in self.second:
            pass
        else:
            self.condition2 = df[self.second][-1+self.secondBefor]

        return float(self.condition1)*self.firstMultiply, float(self.condition2)*self.secondMultiply

    def logic_isBiggerFirst(self,now,df):
        condition1 ,condition2 = self.classification(now,df)
        if condition1 > condition2:
            return True,condition1,condition2
        else:
            return False,condition1,condition2

    def logic_isBiggerSecond(self,now,df):
        condition1 ,condition2 = self.classification(now,df)
        if condition1 < condition2:
            return True,condition1,condition2
        else:
            return False,condition1,condition2

    def logic_plus(self,now,df):
        condition1, condition2 = self.classification(now, df)
        return condition1+condition2 , condition1,condition2

    def logic_minus(self,now,df):
        condition1, condition2 = self.classification(now, df)
        return condition1 - condition2, condition1,condition2

    def logic_mul(self,now,df):
        condition1, condition2 = self.classification(now, df)
        return condition1 * condition2, condition1,condition2

    def logic_div(self,now,df):
        condition1, condition2 = self.classification(now, df)
        return condition1 / condition2, condition1,condition2

    def analysis(self,now,df):
        if self.biggerFirst==0:
            item = self.logic_isBiggerFirst(now,df)
            self.description = str(self.first) +'('+str(item[1]) +') > ' + str(self.second)+'('+str(item[2])+')'
            return item
        elif self.biggerFirst==1:
            item = self.logic_isBiggerSecond(now,df)
            self.description = str(self.first) + '(' + str(item[1]) + ') < ' + str(self.second) + '(' + str(item[2]) + ')'
            return item
        elif self.biggerFirst==2:
            item = self.logic_plus(now, df)
            self.description = str(self.first) + '(' + str(item[1]) + ') + ' + str(self.second) + '(' + str( item[2]) + ') =' + str(item[0])
            return item
        elif self.biggerFirst==3:
            item = self.logic_minus(now, df)
            self.description = str(self.first) + '(' + str(item[1]) + ') - ' + str(self.second) + '(' + str( item[2]) + ') =' + str(item[0])
            return item
        elif self.biggerFirst==4:
            item = self.logic_mul(now, df)
            self.description = str(self.first) + '(' + str(item[1]) + ') * ' + str(self.second) + '(' + str( item[2]) + ') =' + str(item[0])
            return item
        elif self.biggerFirst==5:
            item = self.logic_div(now, df)
            self.description = str(self.first) + '(' + str(item[1]) + ') / ' + str(self.second) + '(' + str( item[2]) + ') =' + str(item[0])
            return item

    def setFirstCondition(self,condition):
        self.condition1 = float(condition)
        print("firstCondition : ",self.logicNumber, self.condition1)

    def setSecondCondition(self,condition):
        self.condition2 = float(condition)
        print("secondCondition : ",self.logicNumber, self.condition2)

    def setTime(self,time):
        self.time= time

    def getTime(self):
        return self.time