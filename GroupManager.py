
from PyQt5.QtWidgets import *
from PyQt5 import uic
import traceback
from upbit_jongmok_popup import UpbitJongmokList
form_class = uic.loadUiType("GROUP.ui")[0]

class GroupManager(QDialog,form_class):
    def __init__(self, groups=None):
        super().__init__()
        self.setupUi(self)
        self.setWindowTitle('그룹 관리')
        self.tab1_btn_group_add.clicked.connect(self.addGroup)
        self.tab1_btn_group_remove.clicked.connect(self.removeGroup)
        self.tab1_btn_group_jong_add.clicked.connect(self.addJongmok)
        self.tab1_btn_group_jong_remove.clicked.connect(self.removeJongmok)
        self.tab1_list_group.itemSelectionChanged.connect(self.selectGroup)
        self.tab1_btn_group_save.clicked.connect(self.save)
        print(groups)
        self.group={}
        if groups != None:
            self.group = groups
            for i in self.group.keys():
                self.tab1_list_group.addItem(i)

    def save(self):
        self.close()
    def addJongmok(self):
        try:
            index = self.tab1_list_group.currentRow()
            item = self.tab1_list_group.item(index)
            groupName = item.text()
        except AttributeError as e:
            QMessageBox.question(self, 'Error', '그룹을 선정해주세요.',QMessageBox.Ok)
            print(traceback.format_exc())
            return
        try:
            dlg = UpbitJongmokList()
            dlg.exec_()
            self.tab1_list_group_jong.addItem(dlg.jongmok)
            self.group[groupName].append(dlg.jongmok)
            print(self.group)
        except:
            print(traceback.format_exc())

    def removeJongmok(self):
        try:
            index = self.tab1_list_group.currentRow()
            item = self.tab1_list_group.item(index)
            groupName = item.text()
            index = self.tab1_list_group_jong.currentRow()
            item = self.tab1_list_group_jong.takeItem(index)
            self.group[groupName].remove(item.text())

            print(self.group)
        except Exception as e:
            print(traceback.format_exc())

    def addGroup(self,cnt = -5):
        try:
            if cnt == False:
                cnt = self.tab1_list_group.count()+1
            print(cnt)
            groupName = 'GROUP'+str(cnt)
            if groupName in self.group.keys():
                self.addGroup(cnt+1)
            else:
                self.tab1_list_group.addItem(groupName)
                self.group[groupName]=[]
        except Exception as e:
            print(traceback.format_exc())

    def removeGroup(self):
        try:
            index = self.tab1_list_group.currentRow()
            item = self.tab1_list_group.item(index)
            groupName = item.text()
            if self.tab1_list_group.count() ==1:
                self.tab1_list_group.clear()
                self.tab1_list_group_jong.clear()
            else:
                index = self.tab1_list_group.currentRow()
                item = self.tab1_list_group.takeItem(index)
            self.group.pop(groupName)
            print(self.group)
        except Exception as e:
            print(traceback.format_exc())

    def selectGroup(self):
        self.tab1_list_group_jong.clear()
        index = self.tab1_list_group.currentRow()
        item = self.tab1_list_group.item(index)
        groupName = item.text()
        if groupName in self.group.keys():
            for i in self.group[groupName]:
                self.tab1_list_group_jong.addItem(i)