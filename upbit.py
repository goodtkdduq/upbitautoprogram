import pyupbit
from MeanMoveLine import MeanMoveLine
from define import Define
class Upbit:
    def __init__(self,jongmok,bong):
        define =Define()
        self.df = pyupbit.get_ohlcv(jongmok, interval=define.bongCheck(bong))
        self.now = pyupbit.get_current_price(jongmok)
        self.df.rename(columns={'open': '시가'}, inplace=True)
        self.df.rename(columns={'high': '고가'}, inplace=True)
        self.df.rename(columns={'low': '저가'}, inplace=True)
        self.df.rename(columns={'close': '종가'}, inplace=True)
        self.df.rename(columns={'volume': '거래량'}, inplace=True)
        info = MeanMoveLine(data=self.df, useVolume=True)

    def getData(self):
        return self.df,self.now