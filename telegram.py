import telepot

class Telegram:

    def __init__(self,token,mc = None):
        #self.token = '1439929447:AAEdveMybmZZV5cJXFUGP-uSVQFbOhzWJHg'
        self.bot = telepot.Bot(token)
        if mc !=None:
            self.mc = mc

    def sendMessage(self,msg):
        self.bot.sendMessage(self.mc,msg)

    def getMessage(self):
        self.mc = None
        update = self.bot.getUpdates()
        print(update)
        for u in update:
            self.mc = u['message']['from']['id']
        if self.mc == None:
            return 'error'
        return self.mc
    
    def sendPhoto(self,path):
        self.bot.sendPhoto(chat_id=self.mc,photo=open(path,'rb'))