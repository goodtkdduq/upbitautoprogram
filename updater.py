import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt
from PyQt5 import uic
import os
import traceback
import time
import requests
import threading
form_class = uic.loadUiType("update.ui")[0]

server = '220.78.10.164'
port = '5050'

class AutoZooZoo(QMainWindow,form_class):
    server = '220.78.10.164'
    port = '5050'
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.update_btn.clicked.connect(self.startProgram)
        self.update_btn_start.clicked.connect(self.startDownload)
        self.update_label.setText('업데이트 버튼을 눌러주세요.')



    def startDownload(self):
        self.update_label.setText('업데이트 중입니다.')
        time.sleep(1)
        th = threading.Thread(target=self.downloadThread)
        th.start()

    def downloadThread(self):
        try:
            print('오잉')
            res = requests.get('http://' + self.server + ':' + self.port + '/123/api/updateFileList')
            fileList = res.text.split('|')
            fileList = fileList[:-1]
            cnt =0
            print(fileList)
            self.update_label_ing.setText(str(cnt)+'/'+str(len(fileList)))
            for file in fileList:
                res = requests.get('http://' + self.server + ':' + self.port + '/123/api/fileUpdate/'+file)
                open('./'+file,'wb').write(res.content)
                cnt+=1
                self.update_prog.setValue(cnt/len(fileList)*100)
                print(cnt/len(fileList)*100)
                self.update_label_ing.setText(str(cnt) + '/' + str(len(fileList)))
            self.update_btn.setEnabled(True)
            self.update_btn_start.setEnabled(False)
            self.update_btn_start.close()
            self.update_label.setText('업데이트 완료하였습니다. 다시 실행주세요.')
            msg = QMessageBox()
            msg.setWindowFlags(Qt.FramelessWindowHint)
            res = msg.warning(self, 'Update', 'Update완료하였습니다.\n프로그램을 재 실행해주세요.', QMessageBox.Ok)
            self.close()
        except Exception as e:
            print(e)


    def startProgram(self):
        try:
            #os.startfile('./main.exe')
            #time.sleep(5)
            app.exit()
        except:
            print(traceback.format_exc())

if __name__ == "__main__":
    app = QApplication(sys.argv)
    import os
    os.environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"
    app.setStyle("Fusion")
    myWindow = AutoZooZoo()
    myWindow.show()
    app.exec_()